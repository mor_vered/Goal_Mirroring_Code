/*
 * Copyright 2017 Mor Vered
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Point.h"
#include <vector>

#ifndef STATE_H_
#define STATE_H_


class State{
private:
	Point currentPosition;
	vector<Point> history;

public:
	//C'tors
	State();
	//Copy C'tor
	State(const State &newState);
	State(Point position);

	//Getters and setters
	const Point& getCurrentPosition() const;
	void setCurrentPosition(const Point& currentPosition);
	vector<Point> &getHistory() {return history; }
	void setHistory(const vector<Point> &newHistory);
	void addToHistory(const Point &newPoint);

	//Assignment overload
	State & operator=(const State &newState);

	//D'tor
	virtual ~State();

};





#endif /* STATE_H_ */
