/*
 * Recognizer.cpp
 *
*
 * Copyright 2017 Mor Vered
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
// Needed includes:
#include <boost/math/distributions/students_t.hpp>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>

#include "Recognizer.h"
#include "math.h"
#include <cmath>
#include <sys/resource.h>
#include <sstream>


// Bring everything into global namespace for ease of use:
using namespace boost::math;
using namespace std;

Recognizer::Recognizer() {
	// TODO Auto-generated constructor stub
	chosenGoal = chooseGoal();
	firstObservation = getInitialPointAndDirection();
	currentState.setCurrentPosition(firstObservation);
	lineLength = 0;
	this->plannerRunTimeLimit = 1;
	this->stop = false;//A boolean that symbolizes we haven't found a path

	initializePossibleGoalsAndGoalLengths();
	planner.setPlannerRunTimeLimit(1);
}


Recognizer::Recognizer(const vector<Goal> &new_Goals)
{
	for(unsigned int i=0;i<new_Goals.size();i++)
		goals.push_back(new_Goals[i]);

	chosenGoal = chooseGoal();
	this->stop = false;//A boolean that symbolizes we haven't found a path
	lineLength = 0;

	this->plannerRunTimeLimit = 1;
	initializePossibleGoalsAndGoalLengths();
	planner.setPlannerRunTimeLimit(1);
}


Recognizer::Recognizer(const vector<Goal> &new_Goals, int plannerIndex, double plannerRunTimeLimit)
{
	this->plannerIndex = plannerIndex;
	for(unsigned int i=0;i<new_Goals.size();i++)
		goals.push_back(new_Goals[i]);
	this->stop = false;//A boolean that symbolizes we haven't found a path
	chosenGoal = chooseGoal();

	lineLength = 0;

	this->plannerRunTimeLimit = plannerRunTimeLimit;
	initializePossibleGoalsAndGoalLengths();
	planner.setPlannerIndex(plannerIndex);
	planner.setPlannerRunTimeLimit(plannerRunTimeLimit);
}



Recognizer::Recognizer(vector<Point> &new_observations, const vector<Goal> &new_Goals)
{
	for(unsigned int i=0;i<new_Goals.size();i++)
		goals.push_back(new_Goals[i]);

	observations = new_observations;
	chosenGoal = chooseGoal();
	firstObservation = getInitialPointAndDirection();
	setCurrentState();
	this->stop = false;//A boolean that symbolizes we haven't found a path
	this->plannerRunTimeLimit = 1;
	initializePossibleGoalsAndGoalLengths();
	planner.setPlannerRunTimeLimit(1);
}

double getLineSize(Point source, Point dest)
{
	double dist = sqrt((dest.getX()-source.getX())*(dest.getX()-source.getX())+
			(dest.getY()-source.getY())*(dest.getY()-source.getY()));
	return dist;

}

void Recognizer::updateObservations(vector<Point> &new_observations)
{
	// *****  I know that the observations are in order *****
	observations = new_observations;
	cout << "New Observations : " << endl;
	//update Directions
	for(unsigned int i=0;i<observations.size()-1;i++)
	{
		cout << observations[i] << endl;
	}
	firstObservation = getInitialPointAndDirection();
	setCurrentState();
}

void Recognizer::addObservations(vector<Point> &new_observations)
{
	observations.push_back(new_observations.back());

	cout << "Adding Observations : " << endl;
	for(unsigned int i=0;i<observations.size();i++)
		cout << "obs. #" << i << " : "  << observations[i] << endl;

	firstObservation = new_observations.front();

	if(firstObservation == observations[observations.size()-1])
	{
		cout << "reached the end or first observation - need to take care of this" << endl;
	}
	setCurrentState();
}


void printLineInformation(double** lineInformation,int numOfLines)
{
	for(int i=0;i<numOfLines;i++)
	{
		cout << "Line " << i << " : " << lineInformation[i][0]<< lineInformation[i][1] << endl;
	}
}

int Recognizer::getPlannerIndex() const {
	return planner.getPlannerIndex();
}

void Recognizer::setPlannerIndex(int plannerIndex) {
	planner.setPlannerIndex(plannerIndex);
	this->plannerIndex = plannerIndex;
}


string Recognizer::getUniqueFileName() const {
	return uniqueFileName;
}

void Recognizer::setUniqueFileName(string uniqueFileName) {
	this->uniqueFileName = uniqueFileName;
}

bool Recognizer::isStop() const {
	return stop;
}

void Recognizer::setStop(bool stop) {
	this->stop = stop;
}

const string& Recognizer::getPathFileName() const {
	return pathFileName;
}

void Recognizer::setPathFileName(const string& pathFileName) {
	this->pathFileName = pathFileName;
}

void Recognizer::setCurrentState()
{
	//Set currentState to be last of observations - and update direction
	int numOfObs = observations.size();
	Point currentPosition;
	//currentState.setHistory(observations);
	if(numOfObs==1)
		currentState.setCurrentPosition(firstObservation);
	else
	{
		vector<Point> newObservations;
		for(int i=0;i<observations.size()-1;i++){
			newObservations.push_back((Point)observations[i]);
		}
		currentState.setHistory(newObservations);
		currentPosition = observations[numOfObs-1];
		currentState.setCurrentPosition(currentPosition);
	}


}





/*
 * A method that extracts the first observation and direction from the observations
 * Still needs to take care of direction...
 */
Point Recognizer::getInitialPointAndDirection()
{
	Point newPoint;
	if(observations.size() > 0)//If I have observations
	{
		newPoint = observations.front();
	}
	return newPoint;
}

/*
 * A method that chooses a goal out of all of the goals by some algorithm
 */
Goal Recognizer::chooseGoal()
{

	if(goals.size() > 0)
	{
		goalIndex = 0;
		return goals.front();
	}
	goalIndex = -1;
	return Goal();
}


void Recognizer::completeParameters()
{

}




void printPath(const vector<Point>& points)
{
	for(unsigned int i=0;i<points.size();++i)
		cout << fixed << setprecision(3) <<points[i].getX() << "," << fixed << setprecision(3) << points[i].getY() << endl;

}



vector<double> Recognizer::getGoalRanking() const {
	return goalRanking;
}

void Recognizer::setGoalRanking(vector<double> goalRanking) {
	this->goalRanking = goalRanking;
}



double Recognizer::calcLength(Point* edgePoints,int numOfEdgePoints)
{
	double length=0;
	for(int i=0;i<numOfEdgePoints-1;++i){
		length += getLineSize(edgePoints[i], edgePoints[i+1]);
	}

	return length;
}




void Recognizer::initializePossibleGoalsAndGoalLengths(){
	for(unsigned int i=0;i<goals.size();++i){
		//vector of possibleGoals all initialized to true and will be updated later by the ranking component
		possibleGoals.push_back(true);
		goalsOptimalLength.push_back(0.0);
		goalRanking.push_back(0.0);//-1 means not possible
									// 0 means not ranked yet
									// positive numbers are the ranking

	}
	goalObsHistoryLength = 0.0;

}

//calculate distance between two 3D points
double Recognizer::dist(Point one, Point two){
	// in case of overflow consult http://stackoverflow.com/questions/14568249/finding-the-distance-between-2-3d-points
	double xSqr = (one.getX() - two.getX()) * (one.getX() - two.getX());
	double ySqr = (one.getY() - two.getY()) * (one.getY() - two.getY());

	double mySqr = xSqr + ySqr; //+ zSqr;

	return sqrt(mySqr);
}


vector<bool> Recognizer::runRecognizer()
{
	bool val;
	vector<double> currentGoalLength(goals.size(),0.0);
	og::PathGeometric* path;

	int timeLimit = (int)this->plannerRunTimeLimit;

	ofstream myFile;
	myFile.open(this->uniqueFileName.c_str(),ios::out | ios::app);

	//Only if this is the first run
	if(currentState.getHistory().size()==0){
		myFile << "plannerIndex TimeLimit Current.x Current.y Current.z Current.az Current.ay Current.az Current.angle NumOfGoals ";
		for(int i=0;i<goals.size();i++){
			myFile << "Goal.x Goal.y Goal.z Goal.ax Goal.ay Goal.az Goal.angle Time CurrentLength ";
		}
		for(int i=0;i<goals.size();i++){
			myFile << "Goal" << i+1 << "Ranking ";
		}
		myFile << endl;
	}

	myFile << planner.getPlannerIndex() << " ";
	myFile << planner.getPlannerRunTimeLimit() << " ";
	myFile << currentState.getCurrentPosition() << " ";
	myFile << this->goals.size() << " ";


	for(unsigned int i=0;i<goals.size()&&!stop;i++)
	{
		//choose next goal
		Goal newGoal = goals[i];
		val = possibleGoals[i];
		string newpathFileName = this->pathFileName +boost::lexical_cast<std::string>(i)+".txt";
		cout << newpathFileName << endl;

		if(val)
		{
			//set next goal in planner
			planner.setGoal(newGoal);
			planner.setStartPoint(currentState.getCurrentPosition());
			planner.setPathFile(newpathFileName);

			path = planner.runPlanner();

			//cout << "Final path size = " << path->length() << endl;
			if(path!=NULL){
				//path->printAsMatrix(cout);
				myFile << goals[i] << " ";
				myFile << planner.getLastPlanTime() << " ";
				myFile << path->length() << " ";

				//If this is the first run need to update the length of each path from start to goal
				if(goalsOptimalLength[i]==0.0)
					goalsOptimalLength[i] = path->length();
				currentGoalLength[i] = path->length(); //length from current point to end

			}
			else{//If the path is NULL
				//myFile << "NO_PATH" << endl;
				myFile << goals[i] << " ";
				myFile << planner.getPlannerRunTimeLimit() << " ";
				myFile << "no_path" << " ";
				possibleGoals[i] = false;
				//stop=true;
			}
		}
		else
		{
			cout << "Impossible" << endl;
		}
	}
	//calculate the History length including the current point
	//TODO change goalObsHistoryLength from vector to single double
	if (currentState.getHistory().size() != 0 )
		goalObsHistoryLength = goalObsHistoryLength + dist(currentState.getHistory().back(),currentState.getCurrentPosition());
	else
		goalObsHistoryLength = 0;
	//push point to history
	currentState.addToHistory(currentState.getCurrentPosition());
	if(!stop){
		rankingComponent(currentGoalLength);

		//print ranking
		for(unsigned int i=0;i<goals.size();++i){
			if(possibleGoals[i])//If it is a possible goal write the ranking
				myFile << goalRanking[i] << " ";
			else//If it is impossible write -1
			{
				myFile << -1 << " ";
				possibleGoals[i] = true;
			}
		}
		myFile << endl;
	}
	myFile.close();
	//fclose (stdout);
	return possibleGoals;
}


//Ranking done according to Elishevas paper
//How long the plan would be compared to how long it could optimally be
void Recognizer::rankingComponent(vector<double> currentGoalLength){
	for(unsigned int i=0;i<goals.size();++i){
		if(possibleGoals[i]){
			double newRanking = goalsOptimalLength[i] / (goalObsHistoryLength + currentGoalLength[i]);

			bool possible = goalElimination(newRanking,i);
			if(possible)
				ranking(newRanking,i);
		}
	}
}

/*
 * For the moment don't eliminate goals
 */
bool Recognizer::goalElimination(double newRanking, int goalIndex){

	return true;
}

//Adjust the actual ranking
void Recognizer::ranking(double newRanking, int goalIndex){
	goalRanking[goalIndex] = newRanking;
	possibleGoals[goalIndex] = true;
}




Recognizer::~Recognizer() {

}









void Recognizer::stateTranslationForPlanner()
{

	  ifstream myReadFile,myReadPointFile;
	  string line,pointLine;
	  Point first,last;


}

double Recognizer::getPlannerRunTimeLimit() const {
	return planner.getPlannerRunTimeLimit();
}

void Recognizer::setPlannerRunTimeLimit(double plannerRunTimeLimit) {
	planner.setPlannerRunTimeLimit(plannerRunTimeLimit);
	this->plannerRunTimeLimit = plannerRunTimeLimit;
}

const State& Recognizer::getCurrentState() const {
	return currentState;
}



const vector<Goal>& Recognizer::getGoals() const {
return goals;
}

void Recognizer::setGoals(const vector<Goal>& goals) {
	this->goals = goals;
	for(int i=0;i<this->goals.size();i++){
		possibleGoals.push_back(1);
		goalsOptimalLength.push_back(0.0);
		goalRanking.push_back(0);
	}
}
