/*
 * Planner.h
 *
 * Copyright 2017 Mor Vered
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PLANNER_H_
#define PLANNER_H_


#include <ompl/base/spaces/SE3StateSpace.h>
#include <ompl/geometric/SimpleSetup.h>
#include <ompl/base/SpaceInformation.h>
#include <ompl/base/ProblemDefinition.h>
#include <ompl/base/ValidStateSampler.h>
#include <ompl/base/samplers/ObstacleBasedValidStateSampler.h>

#include <ompl/geometric/PathGeometric.h>

#include <ompl/tools/benchmark/Benchmark.h>

#include <ompl/geometric/planners/rrt/RRTConnect.h>
#include <ompl/geometric/planners/rrt/RRT.h>
#include <ompl/geometric/planners/kpiece/LBKPIECE1.h>
#include <ompl/geometric/planners/kpiece/KPIECE1.h>
#include <ompl/geometric/planners/est/EST.h>
#include <ompl/geometric/planners/sbl/SBL.h>
#include <ompl/geometric/planners/prm/PRM.h>//optimal
#include <ompl/geometric/planners/prm/PRMstar.h>//optimal
#include <ompl/geometric/planners/rrt/TRRT.h>//optimal
#include <ompl/geometric/planners/rrt/RRTstar.h>//optimal
#include <ompl/geometric/planners/rrt/LBTRRT.h>//semi-optimal
#include <ompl/geometric/planners/prm/SPARS.h> //semi-optimal
#include <ompl/geometric/planners/prm/SPARStwo.h> //semi-optimal
#include <ompl/base/OptimizationObjective.h>
#include <ompl/base/objectives/PathLengthOptimizationObjective.h>


#include <omplapp/apps/SE3RigidBodyPlanning.h>
#include <omplapp/apps/SE2RigidBodyPlanning.h>
#include <omplapp/config.h>
#include <iostream>
#include <string>
#include <cstdio>


#include <iostream>


#include <string>
#include <vector>

#include "Goal.h"
#include "Point.h"

using namespace std;
using namespace ompl;
namespace ob = ompl::base;
namespace og = ompl::geometric;



#define OMPL_LIBRARY_PC "/home/mor/omplapp-1.0.0-Source"
#define OMPL_LIBRARY_LEPTOP "/home/leptop/omplapp-1.0.0-Source"
#define OMPL_LIBRARY OMPL_LIBRARY_LEPTOP
//#define OMPL_LIBRARY "/home/leptop/omplapp-1.0.0-Source" //LEPTOP
//#define ROBOT "/resources/3D/redIrobotSmall.dae" //cubicles_robot.dae" //redIrobotSmall.dae" //spirelli_robot.dae//Apartment_robot.dae"//cubicles_robot.dae
//#define ENV "/resources/3D/Apartment_env.dae" //cubicles_env.dae" //Apartment_env.dae"

#define ENV_CUBE "/resources/3D/cubicles_env.dae"
#define ROBOT_CUBE "/resources/3D/cubicles_robot.dae"
#define ENV_APT "/resources/3D/Apartment_env.dae"
#define ROBOT_APT "/resources/3D/redIrobotSmall.dae"

#define ROBOT ROBOT_CUBE//ROBOT_CUBE //cubicles_robot.dae" //redIrobotSmall.dae" //spirelli_robot.dae//Apartment_robot.dae"//cubicles_robot.dae
#define ENV ENV_CUBE//ENV_CUBE //cubicles_env.dae" //Apartment_env.dae"



class Planner {
private:
	Goal goal;
	string robotModel;
	string envModel;
	int plannerIndex;
	Point startPoint;
	Point endPoint;
	double lastPlanTime;
	double plannerRunTimeLimit;
	string pathFile;


public:
	//C'tor
	Planner();
	Planner(Point startPoint, Point endPoint, string robotModel, string envModel, int plannerIndex=0, double plannerRunTimeLimit=30);
	Planner(Point startPoint, Point endPoint);
	Planner(Point startPoint, Goal goal);
	Planner(Point startPoint);
	Planner(Point startPoint,int plannerIndex,double plannerRunTimeLimit);

	virtual ~Planner();

	og::PathGeometric* runPlanner();

	//Get Set
	const Point& getEndPoint() const;
	void setEndPoint(const Point& endPoint);
	const string& getEnvModel() const;
	void setEnvModel(const string& envModel);
	const Goal& getGoal() const;
	void setGoal(const Goal& goal);
	int getPlannerIndex() const;
	void setPlannerIndex(int plannerIndex);
	const string& getRobotModel() const;
	void setRobotModel(const string& robotModel);
	const Point& getStartPoint() const;
	void setStartPoint(const Point& startPoint);
	double getLastPlanTime() const;
	void setLastPlanTime(double lastPlanTime);
	const string& getPathFile() const;
	void setPathFile(const string& pathFile);
	double getPlannerRunTimeLimit() const;
	void setPlannerRunTimeLimit(double plannerRunTime);
};

#endif /* PLANNER_H_ */
