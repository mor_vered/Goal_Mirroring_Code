/*
 * State.cpp
 *
 *
 * Copyright 2017 Mor Vered
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "State.h"

State::State()
{
	this->currentPosition = Point(0.0,0.0,0.0);
}

//Copy c'tor
State::State(const State &newState)
{
	this->currentPosition = newState.getCurrentPosition();
}

State::State(Point position)
{
	this->currentPosition = position;
}


//Setters and Getters
const Point& State::getCurrentPosition() const {
	return currentPosition;
}

void State::setCurrentPosition(const Point& currentPosition) {
	this->currentPosition = currentPosition;
}

void State::setHistory(const vector<Point> &newHistory)
{
	for(unsigned int i=0;i<newHistory.size();i++)
	{
		this->history.push_back(Point(newHistory[i]));
	}

}


void State::addToHistory(const Point &newPoint)
{
	this->history.push_back(Point(newPoint));
}


//Assignment operator overload
State & State::operator=(const State &newState)
{

	this->currentPosition.setPoint(newState.getCurrentPosition());

	return *this;
}


State::~State() {
	// TODO Auto-generated destructor stub
}


