/*
 * Goal.cpp
 *
 *
 * Copyright 2017 Mor Vered
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Goal.h"
#include <iostream>

Goal::Goal() {
	this->goal.setXYZ(0.0,0.0,0.0);
	this->goal.setRotation(0.0,0.0,0.0,0.0);
}

Goal::Goal(double x, double y, double z, double ax, double ay ,double az, double angle)
{
	this->goal.setXYZ(x,y,z);
	this->goal.setRotation(ax,ay,az,angle);
}

Goal::Goal(Point goal):
		goal(goal){

}

const Point& Goal::getGoal() const {
	return goal;
}

void Goal::setGoal(const Point& goal) {
	this->goal = goal;
}

Goal & Goal::operator=(const Goal &newGoal)
{
	this->goal = newGoal.getGoal();
	return *this;
}

Goal::~Goal() {

}

ostream& operator<<(ostream &out, const Goal &cGoal){
	out <<  cGoal.getGoal();
	return out;
}
