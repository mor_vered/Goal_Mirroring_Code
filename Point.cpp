/*
 * Point.cpp
 *
 * Copyright 2017 Mor Vered
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Point.h"


Point::Point() {
	this->x = 0.0;
	this->y = 0.0;
	this->z = 0.0;
	this->ax = 0.0;
	this->ay = 0.0;
	this->az = 0.0;
	this->angle = 0.0;
}

Point::Point(double newX, double newY, double newZ, double ax, double ay, double az, double angle)
{
	this->x = newX;
	this->y = newY;
	this->z = newZ;
	this->ax = ax;
	this->ay = ay;
	this->az = az;
	this->angle = angle;
}

Point::Point(const Point &newPoint)
{
	this->x = newPoint.getX();
	this->y = newPoint.getY();
	this->z = newPoint.getZ();
	this->ax = newPoint.getAx();
	this->ay = newPoint.getAy();
	this->az = newPoint.getAz();
	this->angle = newPoint.getAngle();
}
/*
Point::Point(const Point &newPoint, const Point &directionPoint)
{
	this->x = newPoint.getX();
	this->y = newPoint.getY();

	this->xDirection = directionPoint.getX() - newPoint.getX();
	this->yDirection = directionPoint.getY() - newPoint.getY();
	//When you want the unit vector divide each component by sqrt ((x2-x1)^2 + (y2-y1)^2)
	double diff = sqrt( xDirection*xDirection + yDirection*yDirection );

	xDirection = xDirection / diff;
	yDirection = yDirection / diff;

	this->size = 1;
}

Point::Point(double newX, double newY, double newXDirection, double newYDirection)
{
	this->x = newX;
	this->y = newY;

	this->xDirection = newXDirection - newX;
	this->yDirection = newYDirection - newY;
	//When you want the unit vector divide each component by sqrt ((x2-x1)^2 + (y2-y1)^2)
	double diff = sqrt( xDirection*xDirection + yDirection*yDirection );

	xDirection = xDirection / diff;
	yDirection = yDirection / diff;

	this->size = 1;

}*/


Point::~Point() {

}


//Getters and Setters

void Point::setX(double x) {
	this->x = x;
}


void Point::setY(double y) {
	this->y = y;
}

void Point::setPoint(Point newP) {
	this->y = newP.getY();
	this->x = newP.getX();
}

// Distance to another point.  Pythagorean thm.
double Point::dist(Point other) {
        double xd = x - other.x;
        double yd = y - other.y;
        double zd = z - other.z;
        return sqrt(xd*xd + yd*yd + zd*zd);
}

// Add or subtract two points.
Point Point::add(Point b)
{
	   return Point(x + b.x, y + b.y,z+b.z);
}

Point Point::sub(Point b)
{
	   return Point(x - b.x, y - b.y,z-b.z);
}

// Move the existing point.
void Point::move(double a, double b, double c)
{
	   x += a;
	   y += b;
	   z += c;
}

// Print the point on the stream.  The class ostream is a base class
// for output streams of various types.
void Point::print(ostream &strm)
{
	  strm << "(" << x << "," << y << "," << z << "," << ax << "," << ay << "," << az << "," << angle << ")";
}

//Assignment operator overload
Point & Point::operator=(const Point &newPoint)
{
	this->x = newPoint.x;
	this->y = newPoint.y;
	this->z = newPoint.z;
	this->ax = newPoint.ax;
	this->ay = newPoint.ay;
	this->az = newPoint.az;
	this->angle = newPoint.angle;
	return *this;
}
/*

double Point::getXDirection() const {
	return xDirection;
}

void Point::setXDirection(double direction) {
	xDirection = direction;
}

double Point::getYDirection() const {
	return yDirection;
}

void Point::setYDirection(double direction) {
	yDirection = direction;
}

void Point::setDirection(Point direction)
{
	xDirection = direction.x;
	yDirection = direction.y;
}

Point Point::getDirectionPoint()
{
	return Point(xDirection,yDirection);
}
*/


bool Point::operator==(const Point &newPoint) const
{
	if( this->x == newPoint.x && this->y == newPoint.y && this->z == newPoint.z && this->ax == newPoint.ax
			&& this->ay == newPoint.ay && this->az == newPoint.az && this->angle==newPoint.angle )
		return true;
	return false;
}

bool Point::operator!=(const Point &newPoint)
{
	return !(*this==newPoint);
}

Point & Point::operator+(const Point &newPoint)
{
	this->x += newPoint.x;
	this->y += newPoint.y;
	this->z += newPoint.z;
	return *this;
}



double Point::getZ() const {
	return z;
}

void Point::setZ(double z) {
	this->z = z;
}

void Point::setXYZ(double x, double y , double z){
	this->x = x;
	this->y = y;
	this->z = z;
}

void Point::setRotation(double ax, double ay, double az, double angle){
	this->ax = ax;
	this->ay = ay;
	this->az = az;
	this->angle = angle;
}

ostream& operator<<(ostream &out, const Point &cPoint){
	out << "(" << cPoint.getX() << "," << cPoint.getY() << "," << cPoint.getZ()
			<< "," << cPoint.getAx() << "," << cPoint.getAy() << "," << cPoint.getAz() << "," << cPoint.getAngle() << ")";
	return out;
}

double Point::getAngle() const {
	return angle;
}

void Point::setAngle(double angle) {
	this->angle = angle;
}

double Point::getAx() const {
	return ax;
}

void Point::setAx(double ax) {
	this->ax = ax;
}

double Point::getAy() const {
	return ay;
}

void Point::setAy(double ay) {
	this->ay = ay;
}

double Point::getAz() const {
	return az;
}

void Point::setAz(double az) {
	this->az = az;
}
