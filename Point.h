/*
 * Point.h
 *
 *
 * Copyright 2017 Mor Vered
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <math.h>

using namespace std;

#ifndef POINT_H_
#define POINT_H_

class Point {
	double x;
	double y;
	double z;
	double ax;
	double ay;
	double az;
	double angle;

public:

	//C'tors
	Point();
	Point(double newX, double newY, double newZ, double ax=0, double ay=0, double az=0, double angle=0);
	Point(const Point &newPoint);
	//Point(double newX, double newY, double newXDirection, double newYDirection);
	//Point(const Point &newPoint, const Point &directionPoint);

	//D'tor
	virtual ~Point();

	//Getters and setters
	double getX() const { return x; }
	void setX(double x);
	double getY() const { return y; }
	void setY(double y);
	double getZ() const;
	void setZ(double z);
	double getAngle() const;
	void setAngle(double angle);
	double getAx() const;
	void setAx(double ax);
	double getAy() const;
	void setAy(double ay);
	double getAz() const;
	void setAz(double az);


	void setPoint(Point newP);
	//double getXDirection() const;
	//void setXDirection(double direction);
	//double getYDirection() const;
	//void setYDirection(double direction);
	//void setDirection(Point direction);
	//Point getDirectionPoint();

	double dist(Point other);
	void print(ostream &strm);
	void move(double a, double b, double c);
	Point sub(Point b);
	Point add(Point b);

	void setXYZ(double x, double y , double z);
	void setRotation(double ax, double ay, double az, double angle);

	//Assignment operator overload
	Point & operator=(const Point &newPoint);
	bool operator==(const Point &newPoint) const;
	bool operator!=(const Point &newPoint);
	Point & operator+(const Point &newPoint);


	friend ostream& operator<<(ostream &out, const Point &cPoint);

};







#endif /* POINT_H_ */
