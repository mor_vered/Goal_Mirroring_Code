Goal Mirroring

Code to goal mirroring recognizer, as was published in http://u.cs.biu.ac.il/~galk/Publications/Papers/acs16.pdf
Written by Mor Vered. 
galk@cs.biu.ac.il
mvered89230@gmail.com

/*
 * Copyright 2017 Mor Vered
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


Prerequisites 

This project was written using the OMPL The Open Motion Planning Library (http://ompl.kavrakilab.org/index.html) and Eclipse Kepler for C++ Developers.
Integration instructions are here : http://ompl.kavrakilab.org/buildSystem.html
You will also need to install Boost


Getting Started 
 
In main.cpp :
#define OMPL_LIBRARY_LABPC "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
#define OMPL_LIBRARY_LEPTOP "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX" 
defines the path to the omplapp library


Program arguments :
1. Planner index,TRRT :7
2. Planner max runtime in seconds: 1
3. File domain location : Cubicles/
4. Path to List of goals : Cubicles/Goals/Goals.txt

5. Path to observation path : Cubicles/OptimalPaths/OptPath.txt
6. Path to save results (connected with file domain location ) : Results

Each path the planner generates is saved in a separate text file which starts with the date and time and ends with 00, 01, 02, ...
The file with just the date and time (and no index at the end ) is a summary of all results.



