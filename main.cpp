/*
 * Copyright 2017 Mor Vered
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "Goal.h"
#include "Point.h"
#include "Recognizer.h"
#include <iostream>
#include <iomanip>
#include <sstream>
#include <locale>
#include <stdio.h>
#include <sys/types.h>
#include <dirent.h>

#include <string>
#include <stdlib.h>
#include <boost/assign/list_of.hpp>

#include <boost/algorithm/string.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/date_time.hpp>
#include <boost/date_time/posix_time/posix_time_io.hpp>
#include <boost/tokenizer.hpp>
#include <boost/algorithm/string/predicate.hpp>
#include <boost/lexical_cast.hpp>

#include <fstream>
#include <sstream>

#include <dirent.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>


#define OMPL_LIBRARY_LABPC "/home/mor/omplapp-1.0.0-Source"
#define OMPL_LIBRARY_LEPTOP "/home/leptop/omplapp-1.0.0-Source"

using namespace boost;
using namespace std;
using namespace ompl;
namespace ob = ompl::base;
namespace og = ompl::geometric;



void print(const vector<Point> &v)
{
	cout << "NEW" << endl;
	for( size_t n = 0; n<v.size(); n++)
		cout << "x = "<< v[n].getX() << " y = " <<
		v[n].getY() << " z =" << v[n].getZ() <<  endl;
}

Point changeDoubleVecToPoint(vector<double> &doubleVec){
	//Go over the vector of doubles and save it as points
		//The order is important
		vector<double>::iterator it = doubleVec.begin();
		return Point(*it,*(it+1),*(it+2),0,0,0,1);
}

void changeDoubleVecToPoints(vector<double> &doubleVec,vector<Point> &pointVec)
{
	//Go over the vector of doubles and save it as points
	//The order is important
	vector<double>::iterator it = doubleVec.begin();
	while(it<doubleVec.end())
	{
		pointVec.push_back(Point(*it,*(it+1),*(it+2),*(it+3),*(it+4),*(it+5),*(it+6)));
		it+=7;
	}
}

void parseInput(vector<double> &doubleVec,string input)
{
    char_separator<char> sep(" ");
    tokenizer< char_separator<char> > tokens(input, sep);

    for (tokenizer< char_separator<char> >::iterator it = tokens.begin();
            it != tokens.end();
            ++it)
    {
        doubleVec.push_back(atof((*it).c_str()));
    }

}

void printGoalRanking(vector<double>& goalRanking){
	for(unsigned int i=0;i<goalRanking.size();++i){
		cout << "Goal index " << i << " ranking is " << goalRanking[i] << endl;
	}

}


/*
 * A method in charge of going over the goal file, reading the possible goals
 * and converting to a Goal Vector
 */
void readGoalsFromFile(vector<Goal>& goalVec,string goalFileName){
	int count=0;
	string input;
	cout << goalFileName << endl;
	ifstream inputFile(goalFileName.c_str());

	if(inputFile.is_open())
	{
		count++;
		//read next observations
		while( getline(inputFile,input) )
		{
			if(input!=""){//ignore a blank line
				vector<double> doubleVec;
				parseInput(doubleVec,input);
				goalVec.push_back(Goal(changeDoubleVecToPoint(doubleVec)));
			}
		}
	}
	else
		cout << "CLOSED" << endl;
}





string currentDateToString(){
	 boost::gregorian::date dayte(boost::gregorian::day_clock::local_day());
	  boost::posix_time::ptime midnight(dayte);
	  boost::posix_time::ptime
	     now(boost::posix_time::microsec_clock::local_time());
	  boost::posix_time::time_duration td = now - midnight;

	  std::stringstream sstream;

	  sstream << dayte.year() << "_" << dayte.month().as_number()
	     << "_" << dayte.day() << "_" << td.hours() << "_" << td.minutes() << "_" << td.seconds();

	  return sstream.str();
}

int howManyFilesInDirectory(string path){
	string newPath = path+"numOfFiles.txt";
	ifstream inputFile(newPath.c_str());
	vector<double> doubleVec;

	if(inputFile.is_open())
	{
		string input;
		//read next observations
		getline(inputFile,input);


				parseInput(doubleVec,input);

	}
	else
		cout << "CLOSED" << endl;

	return (int)doubleVec[0];

}

int countNumOfFilesInDirectory(string path){
	DIR *dp;
	  int i;
	  struct dirent *ep;
	  dp = opendir(path.c_str());

	  if (dp != NULL)
	  {
	    while (ep = readdir (dp)){
	    	ep->d_name;
	      i++;
	    }
	    (void) closedir (dp);
	  }
	  else{
	    perror ("Couldn't open the directory");
	    return -1;
	  }
	  return i;
}





/*  Run Configurations :
*7 //Number of planner - TRRT
*1 // Time limit for planner to run
*LandmarksExperiment/Cubicles/ //Where to save results and get the goals
*Cubicles/OptimalPaths/2/Goals/Goals1.txt //File containing goals
*Cubicles/OptimalPaths/2/Observations/OptPath1.txt //File with path the agent is observing
*Results //Where to save the results
 */
int main(int argc, char *argv[]){

	if(argc < 2){
		cout << "You must provide at least one argument" << endl;
		exit(0);
	}
	//The input to the program
	int plannerIndex = atoi(argv[1]); // 7 TRRT
	double plannerRunTimeLimit = strtod(argv[2],NULL); //1 sec
	string fileDomainLocation = argv[3]; //LandmarksExperiment/Cubicles/
	string goalFileName = argv[4];//fileDomainLocation + argv[4];//"goals.txt";
	string obsFileName = argv[5];//fileDomainLocation + argv[5];//"path.txt" obs.path;
	string outputFileDirectory = fileDomainLocation + argv[6] + "/";
	string pathFileName = fileDomainLocation+argv[6] + "/"+boost::lexical_cast<std::string>(plannerIndex) + "_" + boost::lexical_cast<std::string>(plannerRunTimeLimit) + "_" +currentDateToString();


	string newFilename = outputFileDirectory + boost::lexical_cast<std::string>(plannerIndex) + "_" + boost::lexical_cast<std::string>(plannerRunTimeLimit) + "_" +currentDateToString() + ".txt";
	cout << newFilename << endl;


	string input;
	bool stop=false;
	unsigned int count =0;

	vector<Goal> goalVec;
	readGoalsFromFile(goalVec,goalFileName);


	Planner myPlanner;
	myPlanner.setPlannerIndex(plannerIndex);
	myPlanner.setPlannerRunTimeLimit(plannerRunTimeLimit);

	Recognizer recognizer(goalVec,plannerIndex,plannerRunTimeLimit);
	recognizer.setFileLocation(fileDomainLocation);
	recognizer.setUniqueFileName(newFilename);

	vector <Point> observations;

	//read coordinate observations from input file
	//first three columns represent the 3D position component of the state and the next four columns the orientation represented by a unit quaternion
	ifstream inputFile(obsFileName.c_str());
	if(inputFile.is_open()&&!stop)
	{
		//read next observations
		while( getline(inputFile,input) )
		{
			if (!boost::starts_with(input, "Info")&&!boost::starts_with(input, "Debug")){
				//set the path for the specific path output
				recognizer.setPathFileName(pathFileName + boost::lexical_cast<std::string>(count));
				count++;

				vector<double> doubleVec;
				parseInput(doubleVec,input);
				observations.push_back(changeDoubleVecToPoint(doubleVec));
				recognizer.addObservations(observations);
				recognizer.runRecognizer();

				if(recognizer.isStop())//In case we found no path
				{
					stop = true;
					break;
				}
			}
		}
	}
	return 0;
}

