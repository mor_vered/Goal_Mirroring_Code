/*
 * Recognizer.h
 *
 *
 * Copyright 2017 Mor Vered
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef RECOGNIZER_H_
#define RECOGNIZER_H_



#include <iostream>
#include <vector>
#include <stdlib.h>
#include "Planner.h"
#include "Goal.h"
#include "Point.h"
#include "State.h"


using namespace std;


class Recognizer {
private:
	vector<Goal> goals;
	vector<Point> observations;
	Goal chosenGoal;
	Point firstObservation;
	double lineLength;
	double plannerRunTimeLimit;//in seconds
	int goalIndex;
	int plannerIndex;
	vector<Point> finalPath;
	State currentState;
	vector<double> goalRanking;//GoalRanking according to angles

	vector<bool> possibleGoals;
	vector<double> goalsOptimalLength;
	double goalObsHistoryLength;
	bool stop;

	string fileLocation;
	string uniqueFileName;
	string pathFileName;

	Planner planner;

public:
	Recognizer();
	Recognizer(const vector<Goal> &new_Goals);
	Recognizer(const vector<Goal> &new_Goals, int plannerIndex, double plannerRunTimeLimit);
	Recognizer(vector<Point> &new_observations, const vector<Goal> &new_Goals);
	virtual ~Recognizer();

	Goal chooseGoal(); //A method that chooses a goal out of all of the goals
	void completeParameters();// A method that deduces the size from the observations
	Point getInitialPointAndDirection(); // A method that gets the initial point and direction from the observations
	vector<bool> runRecognizer();// A method in charge of comparing planner output to observations
	void stateTranslationForPlanner();// A method that translates the current world state into state history fit for the planner

	void rankingComponent(vector<double> currentGoalLength);
	bool goalElimination(double newRanking, int goalIndex);
	void ranking(double newRanking, int goalIndex);
	void initializePossibleGoalsAndGoalLengths();
	double dist(Point one, Point two);


	void updateObservations(vector<Point> &new_observations);
	void addObservations(vector<Point> &new_observations);

	vector<double> getGoalRanking() const;
	void setGoalRanking(vector<double> goalRanking);

	double calcLength(Point* edgePoints,int numOfEdgePoints);
	int getPlannerIndex() const;
	void setPlannerIndex(int plannerIndex);
	double getPlannerRunTimeLimit() const;
	void setPlannerRunTimeLimit(double plannerRunTimeLimit);

	const string& getFileLocation() const {
		return fileLocation;
	}

	void setFileLocation(const string& fileLocation) {
		this->fileLocation = fileLocation;
	}

	string getUniqueFileName() const;
	void setUniqueFileName(string uniqueFileName);
	bool isStop() const;
	void setStop(bool stop);
	const string& getPathFileName() const;
	void setPathFileName(const string& pathFileName);
	const vector<Goal>& getGoals() const;
	void setGoals(const vector<Goal>& goals);
	const State& getCurrentState() const;

private:

	void setCurrentState();


};

#endif /* RECOGNIZER_H_ */
