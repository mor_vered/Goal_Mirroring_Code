/*
 * Goal.h
 *
 *
 * Copyright 2017 Mor Vered
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GOAL_H_
#define GOAL_H_

#include <string>
#include <stdlib.h>
#include <vector>
#include <iostream>
#include "Point.h"

using namespace std;

class Goal {
private:
	Point goal;
public:
	Goal();
	Goal(double x, double y, double z, double ax=0, double ay=0,double az=0,double angle=1);
	Goal(Point goal);
	virtual ~Goal();



	Goal & operator=(const Goal &newGoal);
	const Point& getGoal() const;
	void setGoal(const Point& goal);

	friend ostream& operator<<(ostream &out,const Goal &cGoal);
};




#endif /* GOAL_H_ */
