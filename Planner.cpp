/*
 * Planner.cpp
 *
 *
 * Copyright 2017 Mor Vered
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "Planner.h"


Planner::Planner() {
	//default robot model and environment is the piano movers problem
	this->robotModel = std::string(OMPL_LIBRARY) + ROBOT;
	this->envModel = std::string(OMPL_LIBRARY) + ENV;
	//default start and end position
	startPoint.setXYZ(241.81,106.15,36.46);
	endPoint.setXYZ(-31.19,-99.85,36.46);
	//default planner index
	this->plannerIndex = 0;
	//default planner run time of 30 seconds
	this->plannerRunTimeLimit = 30;

	this->lastPlanTime = 0;
}

Planner::Planner(Point startPoint, Point endPoint, string robotModel, string envModel, int plannerIndex,double plannerRunTimeLimit){
	//default robot model and environment is the piano movers problem
	this->robotModel = robotModel;
	this->envModel = envModel;
	//update start and end position
	this->startPoint = startPoint;
	this->endPoint = endPoint;

	this->plannerIndex = plannerIndex;

	this->plannerRunTimeLimit = plannerRunTimeLimit;

	this->lastPlanTime = 0;
}

Planner::Planner(Point startPoint, Point endPoint){
	//default robot model and environment is the piano movers problem
	this->robotModel = std::string(OMPL_LIBRARY) + ROBOT;
	this->envModel = std::string(OMPL_LIBRARY) + ENV;
	//update start and end position
	this->startPoint = startPoint;
	this->endPoint = endPoint;
	//default planner index
	this->plannerIndex = 0;
	//default run time for each planner of 30 seconds
	this->plannerRunTimeLimit = 30;

	this->lastPlanTime = 0;
}

Planner::Planner(Point startPoint, Goal goal){
	//default robot model and environment is the piano movers problem
	this->robotModel = std::string(OMPL_LIBRARY) + ROBOT;
	this->envModel = std::string(OMPL_LIBRARY) + ENV;
	//update start and end position
	this->startPoint = startPoint;
	this->endPoint = goal.getGoal();
	//default planner index
	this->plannerIndex = 0;
	//default run time for each planner of 30 seconds
	this->plannerRunTimeLimit = 30;

	this->lastPlanTime = 0;
}

Planner::Planner(Point startPoint){
	//default robot model and environment is the piano movers problem
	this->robotModel = std::string(OMPL_LIBRARY) + ROBOT;
	this->envModel = std::string(OMPL_LIBRARY) + ENV;
	//update start and end position
	this->startPoint = startPoint;
	this->endPoint.setXYZ(0.0,0.0,0.0);
	//default planner index
	this->plannerIndex = 0;
	//default run time for each planner of 30 seconds
	this->plannerRunTimeLimit = 30;

	this->lastPlanTime = 0;
}

Planner::Planner(Point startPoint, int plannerIndex, double plannerRunTimeLimit){
	//default robot model and environment is the piano movers problem
	this->robotModel = std::string(OMPL_LIBRARY) + ROBOT;
	this->envModel = std::string(OMPL_LIBRARY) + ENV;
	//update start and end position
	this->startPoint = startPoint;
	this->endPoint.setXYZ(0.0,0.0,0.0);

	this->plannerIndex = plannerIndex;
	this->plannerRunTimeLimit = plannerRunTimeLimit;

	this->lastPlanTime = 0;
}


const Point& Planner::getEndPoint() const {
	return endPoint;
}

void Planner::setEndPoint(const Point& endPoint) {
	this->endPoint = endPoint;
}

const string& Planner::getEnvModel() const {
	return envModel;
}

void Planner::setEnvModel(const string& envModel) {
	this->envModel = envModel;
}

const Goal& Planner::getGoal() const {
	return goal;
}

void Planner::setGoal(const Goal& goal) {
	this->goal = goal;
}

int Planner::getPlannerIndex() const {
	return plannerIndex;
}

void Planner::setPlannerIndex(int plannerIndex) {
	this->plannerIndex = plannerIndex;
}

const string& Planner::getRobotModel() const {
	return robotModel;
}

void Planner::setRobotModel(const string& robotModel) {
	this->robotModel = robotModel;
}

const Point& Planner::getStartPoint() const {
	return startPoint;
}

double Planner::getLastPlanTime() const {
	return lastPlanTime;
}

const string& Planner::getPathFile() const {
	return pathFile;
}

void Planner::setPathFile(const string& pathFile) {
	this->pathFile = pathFile;
}

void Planner::setLastPlanTime(double lastPlanTime) {
	this->lastPlanTime = lastPlanTime;
}

void Planner::setStartPoint(const Point& startPoint) {
	this->startPoint = startPoint;
}

/*



//And a state validity checking function defined like this:
  bool isStateValid(const ob::State *state){
	  //If the z is different than the original z, it's an invalid state
	  double* val = static_cast<ob::RealVectorStateSpace::StateType*>(state)->values;
	  if (val[2]!= this->startPoint.getZ() )
		  return false;
	  return true;
  }



   * In order to specify a planner to use a specific ValidStateSampler we define an allocator
   * function that given a pointer to an ompl::base::SpaceInformation, returns ompl::base::ValidStateSamplerPtr
   * (due to thread constraints). In this function we can also configure the valid state sampler
   * according to the specific space information before returning it.

ob::ValidStateSamplerPtr allocOBValidStateSampler(const ob::SpaceInformation* si){
    // we can perform any additional setup / configuration of a sampler here,
    // but there is nothing to tweak in case of the ObstacleBasedValidStateSampler.
	return ob::ValidStateSamplerPtr(new ob::ObstacleBasedValidStateSampler(si));
}




*/



//And a state validity checking function defined like this:
//http://ompl.kavrakilab.org/StateSampling_8cpp_source.html
  bool isStateValid(const ob::State *state){

	return true;
  }


  // Returns a structure representing the optimization objective to use
  // for optimal motion planning. This method returns an objective which
  // attempts to minimize the length in configuration space of computed
  // paths. http://ompl.kavrakilab.org/optimalPlanningTutorial.html
  ob::OptimizationObjectivePtr getPathLengthObjective(const ob::SpaceInformationPtr& si)
  {
      return ob::OptimizationObjectivePtr(new ob::PathLengthOptimizationObjective(si));
  }




/*
 * 0 RRTConnect
 * 1 LBKPIECE1
 * 2 SBL
 * 3 KPIECE1
 * 4 PRMstar
 * 5 RRTstar
 * 6 PRM
 * 7 TRRT
 * 8 SPARS
 * 9 SPARStwo
 * 10 LBTRRT
 * 11 RRT
 * DEFAULT RRT
 *
 */
og::PathGeometric* Planner::runPlanner(){

	app::SE3RigidBodyPlanning setup;//3D
	//app::SE2RigidBodyPlanning setup;//2D
	bool solved;

	//load the robot and environment
	setup.setRobotMesh(this->robotModel.c_str());
	setup.setEnvironmentMesh(this->envModel.c_str());

	if (this->envModel==std::string(OMPL_LIBRARY) + ENV_APT){
		// Bounds for Apartment environment
		base::RealVectorBounds bounds(3);
		bounds.low[0] = -73.76;
		bounds.low[1] = -179.59;
		bounds.low[2] = -0.03;
		bounds.high[0] = 295.77;
		bounds.high[1] = 168.26;
		bounds.high[2] = 3;


		// Bound the state space
		setup.getSpaceInformation ()->getStateSpace ()->as <base::SE3StateSpace> ()->setBounds (bounds);
	}
	/*
	 * The default behaviour for ompl::base::PathLengthOptimizationObjective is to set the threshold to 0.0 if a threshold wasn't specified.
	 * If you don't specify a threshold, we assume that you want the planner to return the best possible path it can find in the time limit.
	 * Therefore, setting a threshold of 0.0 means that the objective will never be satisfied, and guarantee that the planner runs all the way
	 * until the specified time limit and returns the best path it could find.
	 */
	setup.setOptimizationObjective(getPathLengthObjective(setup.getSpaceInformation()));

	//define start state
	base::ScopedState<base::SE3StateSpace> start(setup.getSpaceInformation());//3D
	//base::ScopedState<base::SE2StateSpace> start(setup.getSpaceInformation());//2D

	start->setXYZ(this->startPoint.getX(),this->startPoint.getY(),this->startPoint.getZ());
	start->rotation().setAxisAngle(this->startPoint.getAx(),this->startPoint.getAy(),this->startPoint.getAz(),this->startPoint.getAngle());


	//define goal state
	base::ScopedState<base::SE3StateSpace> goal(setup.getSpaceInformation());//goal(start); 3D
	//base::ScopedState<base::SE2StateSpace> goal(setup.getSpaceInformation());//2D

	goal->setXYZ(this->goal.getGoal().getX(),this->goal.getGoal().getY(),this->goal.getGoal().getZ());
	//goal->setXY(this->goal.getGoal().getX(),this->goal.getGoal().getY());


	goal->rotation().setIdentity();
	goal->rotation().setAxisAngle(this->goal.getGoal().getAx(),this->goal.getGoal().getAy(),this->goal.getGoal().getAz(),this->goal.getGoal().getAngle());
	//set the start and goal states
	setup.setStartAndGoalStates(start, goal);


	//setup collision checking resolution to 1% of the space extent // 0.01
	setup.getSpaceInformation()->setStateValidityChecker(boost::bind(&isStateValid, _1));
	setup.getSpaceInformation()->setStateValidityCheckingResolution(0.01);



	switch(plannerIndex){
		case 1:{
			setup.setPlanner(base::PlannerPtr(new og::RRTConnect(setup.getSpaceInformation())));
			break;
		}
		case 2:{
			setup.setPlanner(base::PlannerPtr(new og::LBKPIECE1(setup.getSpaceInformation())));
			break;
		}
		case 3:{
			setup.setPlanner(base::PlannerPtr(new og::RRTstar(setup.getSpaceInformation())));
			break;
		}
		case 4:{
			setup.setPlanner(base::PlannerPtr(new og::PRMstar(setup.getSpaceInformation())));
			break;
		}
		case 5:{
			setup.setPlanner(base::PlannerPtr(new og::SBL(setup.getSpaceInformation())));
			break;
		}
		case 6:{
			setup.setPlanner(base::PlannerPtr(new og::PRM(setup.getSpaceInformation())));
			break;
		}
		case 7:{
			setup.setPlanner(base::PlannerPtr(new og::TRRT(setup.getSpaceInformation())));
			break;
		}
		case 8:{
			setup.setPlanner(base::PlannerPtr(new og::SPARS(setup.getSpaceInformation())));
			break;
		}
		case 9:{
			setup.setPlanner(base::PlannerPtr(new og::SPARStwo(setup.getSpaceInformation())));
			break;
		}
		case 10:{
			setup.setPlanner(base::PlannerPtr(new og::LBTRRT(setup.getSpaceInformation())));
			break;
		}
		case 11:{
			setup.setPlanner(base::PlannerPtr(new og::RRT(setup.getSpaceInformation())));
			break;
		}
		case 12:{
			setup.setPlanner(base::PlannerPtr(new og::KPIECE1(setup.getSpaceInformation())));
			break;
		}
		default:{
			setup.setPlanner(base::PlannerPtr(new og::RRTstar(setup.getSpaceInformation())));
		}
	}


	//change the output to a file called path.txt and not stdout. matrix starts after four lines
	//freopen(pathFile.c_str(),"w",stdout);
	ofstream myFile;
	myFile.open(this->pathFile.c_str(),ios::out | ios::app);


	if(setup.solve(this->plannerRunTimeLimit)){

		og::PathGeometric* newPath = new og::PathGeometric(setup.getSolutionPath());
		double computationTime  = setup.getLastPlanComputationTime();
		double simplifyTime = setup.getLastSimplificationTime();
		lastPlanTime = computationTime + simplifyTime;

		if(newPath->getStateCount()<20){
			newPath->interpolate(20);
		}

		newPath->printAsMatrix(cout);
		newPath->printAsMatrix(myFile);
		myFile.close();
		return newPath;
	}
	else{//In case a path was not found return NULL
		myFile << "NO PATH" << endl;
		myFile.close();
		return NULL;
	}
}

double Planner::getPlannerRunTimeLimit() const{

	return this->plannerRunTimeLimit;
}


void Planner::setPlannerRunTimeLimit(double plannerRunTimeLimit){
	this->plannerRunTimeLimit = plannerRunTimeLimit;
}

Planner::~Planner() {
	// TODO Auto-generated destructor stub
}

